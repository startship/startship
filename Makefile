export $UID = $(id -u)
export $GID = $(id -g)

makemigrations:
	docker-compose run --rm api bash -ci 'python manage.py makemigrations'

migrate:
	docker-compose run --rm api bash -ci 'python manage.py migrate'

createsuperuser:
	docker-compose run --rm api bash -ci 'python manage.py createsuperuser --email admin@example.com --username admin'

install:
	docker-compose run --rm api bash -ci 'pip install -r requirements.txt' && docker-compose run --rm frontend bash -ci 'rm -fr node_modules && npm install'

collectstatic:
	docker-compose run --rm api bash -ci 'python manage.py collectstatic'

indexdata:
	docker-compose run --rm api bash -ci 'python manage.py indexdata && python manage.py search_index --rebuild'

restart:
	docker-compose restart

build-frontend:
	docker-compose run --rm frontend bash -ci 'npm run build'

rebuildindex:
	docker-compose exec api python manage.py search_index --rebuild
