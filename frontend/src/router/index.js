import Vue from 'vue'
import Router from 'vue-router'
import Home from '../views/Home.vue'
import Watchlist from '../views/Watchlist.vue'
import Company from '../views/Company.vue'
import News from '../views/news.vue'
import NotFounds from '@/views/NotFounds.vue'
import Messages from '@/views/Messages.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/watchlist',
      name: 'Watchlist',
      component: Watchlist
    },
    {
      path: '/news',
      name: 'News',
      component: News
    },
    {
      path: '/company/:id',
      name: 'Detail',
      component: Company
    },
    {
      path: '/not-founds',
      name: 'Not Founds',
      component: NotFounds
    },
    {
      path: '/messages',
      name: 'Messages',
      component: Messages
    }
  ]
})
