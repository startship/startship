import {api} from '../../api'

const ADD_TO_NOT_FOUNDS = 'ADD_TO_NOT_FOUNDS'
const REMOVE_NOT_FOUND = 'REMOVE_NOT_FOUND'

const state = {
  notFounds: []
}

const mutations = {
  [ADD_TO_NOT_FOUNDS] (state, payload) {
    state.notFounds.push(payload.notFound)
  },

  [REMOVE_NOT_FOUND] (state, payload) {
    state.notFounds.splice(state.notFounds.indexOf(payload.notFound), 1)
  }
}

const actions = {

  addToNotFounds ({state, getters, commit, rootState, rootGetters}, notFound) {
    commit({
      type: ADD_TO_NOT_FOUNDS,
      notFound
    })
  },

  removeNotFound ({state, getters, commit, rootState, rootGetters}, notFound) {
    commit({
      type: REMOVE_NOT_FOUND,
      notFound
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
