import { api } from '../../api'

const START_LOADING = 'START_LOADING'
const FETCH_COMPANIES_SUCCESS = 'FETCH_COMPANIES_SUCCESS'
const FETCH_COMPANIES_FAIL = 'FETCH_COMPANIES_FAIL'
const SUGGEST_COMPANIES_SUCCESS = 'SUGGEST_COMPANIES_SUCCESS'
const SUGGEST_COMPANIES_FAIL = 'SUGGEST_COMPANIES_FAIL'
const START_LOADING_SUGGESTS = 'START_LOADING_SUGGESTS'
const APPLY_FILTER = 'APPLY_FILTER'
const SELECT_COMPANY = 'SELECT_COMPANY'
const FETCH_COUNTRIES_SUCCESS = 'FETCH_COUNTRIES_SUCCESS'
const SET_SEARCH_TEXT = 'SET_SEARCH_TEXT'
const FETCH_CATEGORY_GROUPS_SUCCESS = 'FETCH_CATEGORY_GROUPS_SUCCESS'

const state = {
  searchText: '',
  companies: [],
  suggestions: {
    term_suggests: [],
    completion_suggests: []
  },
  loading: false,
  loadingSuggests: false,
  errorMessage: '',
  filter: {
    countries: null,
    cityFilterValue: null,
    ageFilterValue: null,
    lastFoundedDateFilterValue: null,
    lastFoundedAmountFilterValue: null,
    technologyFilterValue: null,
    sectorFilterValue: null
  },
  company: {},
  countries: [],
  categoryGroups: [],
}

const mutations = {
  [START_LOADING] (state) {
    state.loading = true
  },

  [FETCH_COMPANIES_SUCCESS] (state, payload) {
    state.loading = false
    state.companies = payload.companies
  },

  [FETCH_COMPANIES_FAIL] (state, payload) {
    state.loading = false
    state.errorMessage = payload
  },

  [START_LOADING_SUGGESTS] (state) {
    state.loadingSuggests = true
  },

  [SUGGEST_COMPANIES_SUCCESS] (state, payload) {
    state.loadingSuggests = false
    state.suggestions = payload.suggestions
  },

  [SUGGEST_COMPANIES_FAIL] (state, payload) {
    state.loadingSuggests= false
    state.errorMessage = payload
  },

  [APPLY_FILTER] (state, payload) {
    state.filter = {...state.filter, ...payload.filter}
  },

  [SELECT_COMPANY] (state, payload) {
    state.company = payload.company
  },

  [FETCH_COUNTRIES_SUCCESS] (state, payload) {
    state.countries = payload.countries
  },

  [SET_SEARCH_TEXT] (state, payload) {
    state.searchText = payload.text
  },

  [FETCH_CATEGORY_GROUPS_SUCCESS] (state, payload) {
    state.categoryGroups = payload.categoryGroups
  }
}

const actions = {
  fetchAll ({state, getters, commit, rootState, rootGetters}) {
    commit({
      type: START_LOADING
    })
    return api.fetchAll()
      .then((companies) => {
        state.companies = companies
      })
  },

  search ({state, getters, commit, rootState, rootGetters}, text) {
    commit({
      type: START_LOADING
    })
    commit({
      type: SET_SEARCH_TEXT,
      text
    })
    return api.search(text, state.filter)
      .then(companies => commit({
        type: FETCH_COMPANIES_SUCCESS,
        companies
      }))
      .catch(error => commit({
        type: FETCH_COMPANIES_FAIL,
        payload: error
      }))
  },

  suggest ({state, getters, commit, rootState, rootGetters}, text) {
      commit({
        type: START_LOADING_SUGGESTS
      })
      return api.suggest(text)
        .then(suggestions => commit({
          type: SUGGEST_COMPANIES_SUCCESS,
          suggestions
        }))
        .catch(error => commit({
          type: SUGGEST_COMPANIES_FAIL,
          payload: error
        }))
  },

  setFilterValues({state, getters, commit, rootState, rootGetters}, filter){
      commit({
        type: APPLY_FILTER,
        filter
      })
      return api.search(state.searchText, filter)
        .then(companies => commit({
          type: FETCH_COMPANIES_SUCCESS,
          companies
        }))
      .catch(error => commit({
        type: FETCH_COMPANIES_FAIL,
        payload: error
      }))
  },

  selectCompany ({state, getters, commit, rootState, rootGetters}, company){
    commit({
      type: SELECT_COMPANY,
      company
    })
  },

  fetchCountries ({state, getters, commit, rootState, rootGetters}) {
    api.fetchCountries()
      .then(countries => {
        commit({
          type: FETCH_COUNTRIES_SUCCESS,
          countries
        })
      })
      .catch(console.log)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
