export const TOGGLE_RIGHT_SIDEBAR = 'TOGGLE_RIGHT_SIDEBAR'
export const TOGGLE_LEFT_SIDEBAR = 'TOGGLE_LEFT_SIDEBAR'
export const TOGGLE_LEFT_DRAWER = 'TOGGLE_LEFT_DRAWER'
export const TOGGLE_RIGHT_DRAWER = 'TOGGLE_RIGHT_DRAWER'

const state = {
  rightSidebarOpened: true,
  leftSidebarOpened: true,
  rightDrawer: null,
  leftDrawer: null
}

const mutations = {
  [TOGGLE_RIGHT_SIDEBAR] (state) {
    state.rightSidebarOpened = !state.rightSidebarOpened
  },
  [TOGGLE_LEFT_SIDEBAR] (state) {
    state.leftSidebarOpened = !state.leftSidebarOpened
  },
  [TOGGLE_LEFT_DRAWER] (state) {
    state.leftDrawer = !state.leftDrawer
  },
  [TOGGLE_RIGHT_DRAWER] (state) {
    state.rightDrawer = !state.rightDrawer
  }
}

const actions = {
  toggleRightSidebar ({ commit }) {
    commit({ type: TOGGLE_RIGHT_SIDEBAR })
  },
  toggleLeftSidebar ({ commit }) {
    commit({ type: TOGGLE_LEFT_SIDEBAR })
  },
  toggleLeftDrawer ({ commit }) {
    commit({ type: TOGGLE_LEFT_DRAWER })
  },
  toggleRightDrawer ({ commit }) {
    commit({ type: TOGGLE_RIGHT_DRAWER })
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}
