import {api} from '../../api'

const START_LOADING_WATCHLIST = 'START_LOADING_WATCHLIST'
const FETCH_WATCHLIST_SUCCESS = 'FETCH_WATCHLIST_SUCCESS'
const FETCH_WATCHLIST_FAIL = 'FETCH_WATCHLIST_FAIL'
const WATCHLIST_ADD = 'WATCHLIST_ADD'
const WATCHLIST_REMOVE = 'WATCHLIST_REMOVE'

const state = {
  watchlist: [],
  loading: false,
  errorMessage: ''
}

const mutations = {
  [START_LOADING_WATCHLIST] (state) {
    state.loading = true
  },

  [FETCH_WATCHLIST_SUCCESS] (state, payload) {
    state.loading = false
    state.watchlist = payload.watchlist
  },

  [FETCH_WATCHLIST_FAIL] (state, payload) {
    state.loading = false
    state.errorMessage = payload
  },

  [WATCHLIST_ADD] (state, payload) {
    state.watchlist.push(payload.company)
  },

  [WATCHLIST_REMOVE] (state, payload) {
    var index = state.watchlist.indexOf(payload.company)
    if(index != -1) {
      state.watchlist.splice(index, 1);
    }
  },

}

const actions = {
  fetchWatchlist ({state, getters, commit, rootState, rootGetters}) {
    commit({
      type: START_LOADING_WATCHLIST
    })
    return api.fetchAll()
      .then((companies) => {
        state.watchlist = companies
      })
  },

  addToWatchlist ({state, getters, commit, rootState, rootGetters}, company) {
    commit({
      type: WATCHLIST_ADD,
      company
    })
  },

  removeFromWatchlist ({state, getters, commit, rootState, rootGetters}, company) {
    commit({
      type: WATCHLIST_REMOVE,
      company
    })
  },

}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
