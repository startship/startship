import {api} from '../../api'

const START_LOADING = 'START_LOADING'
const FETCH_TRENDRADAR_SUCCESS = 'FETCH_TRENDRADAR_SUCCESS'
const FETCH_TRENDRADAR_FAIL = 'FETCH_TRENDRADAR_FAIL'
const APPLY_TRENDRADAR_CHANGES = 'APPLY_TRENDRADAR_CHANGES'

const state = {
  chartProperties: {
    labels: [
        'Software Engeneering',
        'Data Analytics',
        'Data Management',
        'Network Administration'
    ],
    datasets: [
        {
            label: "2017",
            data: [18, 10, 12, 8],
            pointBackgroundColor: "green",
            pointBorderColor: "green",
            pointRadius: 4,
            backgroundColor: "green",
            fill: false,
            borderColor: "transparent"
        },
        {
            label: "2018",
            data: [52, 19, 21, 33],
            pointBackgroundColor: "red",
            pointBorderColor: "red",
            pointRadius: 7,
            backgroundColor: "red",
            fill: false,
            borderColor: "transparent"
        }
    ]
},
  loading: false,
  errorMessage: ''
}

const mutations = {
    [START_LOADING] (state) {
        state.loading = true
    },
    [FETCH_TRENDRADAR_FAIL] (state, data) {
        state.loading = false
        state.errorMessage = data
    },
    [FETCH_TRENDRADAR_SUCCESS] (state, data) {
        state.loading = false
        state.errorMessage = ''
        state.chartProperties = data
    },
    [APPLY_TRENDRADAR_CHANGES] (state, data) {
        state.loading = false
        state.errorMessage = ''
        state.chartProperties = data.chartProperties
    }

}

const actions = {
  initTrendradar ({state, getters, commit, rootState, rootGetters}) {
    commit({
      type: START_LOADING
    })
    const data = api.fetchTrendradarData()
    commit({
        type: FETCH_TRENDRADAR_SUCCESS,
        data
    })
  },
  changeChartProperties ({state, getters, commit, rootState, rootGetters}, data) {
    commit({
        type: APPLY_TRENDRADAR_CHANGES,
        data
    })
  },

}

const getters = {
    getChartProperties: state => {
        console.log('GETTER ' , state)
        return state.chartProperties
    }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
