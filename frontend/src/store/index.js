import Vuex from 'vuex'
import Vue from 'vue'
import companies from './modules/companies'
import watchlist from './modules/watchlist'
import trendradar from './modules/trendradar'
import ui from './modules/ui'
import notFounds from './modules/notFounds'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    watchlist,
    companies,
    trendradar,
    ui,
    notFounds
  },
  state: {},
  getters: {},
  actions: {},
  mutations: {}
})
