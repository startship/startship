import axios from 'axios'

const API_URL = 'http://localhost:8001/api/'
const api = {
  companiesUrl: API_URL + 'companies/',

  fetchAll () {
    return axios.get(this.companiesUrl)
      .then(response => response.data)
      .catch(console.log)
  },

  search (text, filter) {
    let searchUrl = this.companiesUrl + '?search=' + text
    if(filter.countries){
      searchUrl += "&country=" + filter.countries.join(',')
    }
    if(filter.categoryGroups){
      searchUrl += "&category_groups=" + filter.categoryGroups.join(',')
    }
    return axios.get(searchUrl)
      .then(response => response.data)
      .catch(console.log)
  },

  suggest (text) {
    return axios.get(this.companiesUrl + '?suggest=' + text)
      .then(response => response.data)
      .catch(console.log)
  },

  fetchCountries () {
    return axios.get(API_URL + 'countries/')
      .then(response => response.data)
      .catch(console.log)
  },

  fetchTrendradarData(){
    return {
      labels: ['Software Engeneering', 'Data Analytics', 'Data Management', 'Network Administration'],
      datasets: [{
          label: "2017",
          data: [18, 10, 12, 8],
          pointBackgroundColor: "green",
          pointBorderColor: "green",
          pointRadius: 4,
          backgroundColor: "green",
          fill: false,
          borderColor: "transparent"
      },
      {
          label: "2018",
          data: [52, 19, 21, 33],
          pointBackgroundColor: "red",
          pointBorderColor: "red",
          pointRadius: 7,
          backgroundColor: "red",
          fill: false,
          borderColor: "transparent"
      }]
    }
  },

  fetchCategoryGroups () {
    return axios.get(API_URL + 'category-groups/')
      .then(response => response.data)
      .catch(console.log)
  },
}

export { api, API_URL }
