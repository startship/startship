from django.http import HttpResponse
from django.template import loader
import settings

def index(request):
    settings.DEBUG
    template = loader.get_template("startship/index.html")
    return HttpResponse(template.render())
