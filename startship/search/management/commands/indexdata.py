import os
import csv
import re

from django.core.management import BaseCommand

from startship.search.models import Company, Location, Category, CategoryGroup


# The class must be named Command, and subclass BaseCommand
class Command(BaseCommand):

    # Show this when the user types help
    help = "Indexing data by importing from .csv file"

    def create_location(self, city, area, country, continent):
        try:
            location = Location.objects.get(city=city, area=area, country=country, continent=continent)
        except Exception:
            Location(
                city=city,
                area=area,
                country=country,
                continent=continent
            ).save()
            location = Location.objects.get(city=city, area=area, country=country, continent=continent)

        return location

    def create_category(self, name):
        try:
            category = Category.objects.get(name=name)
        except Exception:
            Category(name=name).save()
            category = Category.objects.get(name=name)

        return category

    def create_category_group(self, name):
        try:
            category_group = CategoryGroup.objects.get(name=name)
        except Exception:
            CategoryGroup(name=name).save()
            category_group = CategoryGroup.objects.get(name=name)

        return category_group


    def handle(self, *args, **options):
        PROJECT_PATH = os.path.abspath(os.path.dirname(__name__))
        self.stdout.write('indexing data...')

        with open(PROJECT_PATH + '/startship/search/csv_data/companies.csv', 'r') as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',', quotechar='|')
            for row in csv_reader:

                locations = row[17].split(';')
                categories_list = row[15].split(';')
                categories_group_list = row[16].split(';')

                if len(locations) == 1:
                    continue

                company = Company(
                    name=row[1],
                    cb_url=row[2],
                    type=row[3],
                    short_description=row[4],
                    status=row[5],
                    homepage=row[6],
                    founded=row[10] if row[10] else '2017-11-01',
                    last_funding_on=row[12],
                    funding_total_usd=row[13],
                    funding_rounds=row[18]
                )

                self.stdout.write('indexing: ' + company.name)

                company.save()

                city = locations[0]
                area = locations[1]
                country = locations[2].strip()
                continent = locations[3]
                location = self.create_location(city, area, country, continent)
                company.location.add(location.id)

                for name in categories_list:
                    category = self.create_category(name.strip())
                    company.category_list.add(category)

                for name in categories_group_list:
                    category = self.create_category_group(name.strip())
                    company.category_group_list.add(category)
                
        self.stdout.write("Companies indexed")



