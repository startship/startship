from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.name


class CategoryGroup(models.Model):
    name = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.name


class Location(models.Model):
    city = models.CharField(max_length=255, null=True)
    area = models.CharField(max_length=255, null=True)
    country = models.CharField(max_length=255, null=True)
    continent = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.city + ', ' + self.area + ', ' + self.country + ', ' + self.continent


class Company(models.Model):
    name = models.CharField(max_length=255, null=True)
    cb_url = models.CharField(max_length=255, null=True)
    type = models.CharField(max_length=255, null=True)
    short_description = models.TextField(null=True)
    status = models.CharField(max_length=255, null=True)
    homepage = models.CharField(max_length=255, null=True)
    founded = models.CharField(max_length=255, null=True, default='Not known')
    last_funding_on = models.CharField(max_length=255, null=True)
    category_list = models.ManyToManyField(Category, blank=True, related_name='company')
    category_group_list = models.ManyToManyField(CategoryGroup, blank=True, related_name='company')
    location = models.ManyToManyField(Location, blank=True, related_name='company')
    created = models.DateTimeField(auto_now_add=True, null=True)
    funding_total_usd = models.CharField(max_length=255, null=True)
    funding_rounds = models.CharField(max_length=255, null=True)


    def __str__(self):
        return self.name
