from django.contrib import admin
from startship.search.models import Company, Location, Category, CategoryGroup

admin.site.register(Company)
admin.site.register(Location)
admin.site.register(Category)
admin.site.register(CategoryGroup)
