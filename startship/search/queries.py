def common_query(text):
    return {
        "from": 0, "size": 100,
        "query": {
            "bool": {
                "must": [
                    {
                        "nested": {  # For filtering based on nested object
                            "path": "location",
                            "query": {
                                "bool": {
                                    "should": [],
                                    "minimum_should_match": 1
                                }
                            }
                        }
                    }
                ],
                "should": [
                    {
                        "nested": {
                            "path": "location",
                            "query": {
                                "multi_match": {
                                    "query": text,
                                    "fields": [
                                        "location.country^7",
                                        "location.city^7",
                                        "location.area^7",
                                        "location.continent^7"
                                    ]
                                }
                            }
                        }
                    },
                    {
                        "multi_match": {
                            "query": text,
                            "type": "best_fields",
                            "fields": [
                                "name^8",
                                "short_description^10",
                                "category_group_list^10",
                                "category_list^10"
                            ]
                        }
                    },
                    {
                        "common": {
                            "short_description": {
                                "query": text,
                                "cutoff_frequency": 0.001,
                                "minimum_should_match": 2,
                                "boost": 10
                            }
                        }
                    },
                    {
                        "common": {
                            "category_list": {
                                "query": text,
                                "cutoff_frequency": 0.001,
                                "minimum_should_match": 1,
                                "boost": 8
                            }
                        }
                    }
                ],
                "minimum_should_match": 1
            }
        }
    }


def completion_suggest_query(text):
    return {
        "suggest": {
            "text": text,
            "completion_category_group_suggest": {
                "completion": {
                    "field": "category_group_list.name.raw",
                    "skip_duplicates": "true"
                }
            },
            "completion_category_suggest": {
                "completion": {
                    "field": "category_list.name.raw",
                    "skip_duplicates": "true"
                }
            },
            "completion_name_suggest": {
                "completion": {
                    "field": "name.raw",
                    "skip_duplicates": "true"
                }
            }
        }
    }


def term_suggest_query(text):
    return {
      "suggest": {
          "text": text,
          "term_suggest": {
              "term": {
                  "field": "name"
              }
          }
      }
    }
