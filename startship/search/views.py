import logging

from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response
from elasticsearch import Elasticsearch
from .serializers import UserSerializer, GroupSerializer, CompanySerializer, LocationSerializer, CountrySerializer, CategorySerializer, CategoryGroupSerializer
from .models import Company, Location, Category, CategoryGroup
from .queries import common_query, completion_suggest_query, term_suggest_query
from pprint import pprint

es = Elasticsearch(['elastic'], port=9200)

logger = logging.getLogger(__name__)


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class CompanyViewSet(viewsets.ModelViewSet):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer


class LocationViewSet(viewsets.ModelViewSet):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer


class CountryViewSet(viewsets.ModelViewSet):
    queryset = Location.objects.distinct('country')
    serializer_class = CountrySerializer


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class CategoryGroupViewSet(viewsets.ModelViewSet):
    queryset = CategoryGroup.objects.all()
    serializer_class = CategorySerializer


@api_view(['GET'])
def search_companies(request):
    search_text = request.GET.get('search', None)
    suggest_text = request.GET.get('suggest', None)
    country = request.GET.get('country', None)
    query_filter = {'country': country}

    data = []
    if suggest_text:
        data = suggest(suggest_text)
    if search_text:
        data = search(search_text, query_filter)

    return Response(data)


# Helper functions

def apply_country_filter(query, country_filter):
    if not country_filter:
        return query

    countries = country_filter.split(',')
    for country in countries:
        query["query"]["bool"]["must"][0]["nested"]["query"]["bool"]["should"].append({
            "match": {
                "location.country.keyword": country
            }
        })

    return query


def apply_age_filter(query, age_filter):
    if not age_filter:
        return query

    return query


def add_id_and_score_to_company(hit):
    id = hit['_id']
    score = hit['_score']
    company = hit['_source']
    company['id'] = id
    company['score'] = score

    return company


# Search and Suggest

def search(text, filter):
    common = apply_age_filter(
        apply_country_filter(
            common_query(text),
            filter['country']
        ),
        filter['country']
    )
    common_terms_query_res = es.search(index='companies', body=common)
    companies = map(add_id_and_score_to_company, common_terms_query_res['hits']['hits'])

    return companies


def suggest(text):
    completion_response = es.search(index='companies', body=completion_suggest_query(text))
    term_response = es.search(index='companies', body=term_suggest_query(text))
    term_suggests = term_response['suggest']['term_suggest'][0]['options']
    completion_cateogry_suggest = completion_response['suggest']['completion_category_suggest'][0]['options']
    completion_cateogry_group_suggest = completion_response['suggest']['completion_category_group_suggest'][0]['options']
    completion_name_suggest = completion_response['suggest']['completion_name_suggest'][0]['options']

    completion_suggests = completion_cateogry_suggest + completion_cateogry_group_suggest + completion_name_suggest
    suggests = {
        'term_suggests': term_suggests,
        'completion_suggests': completion_suggests
    }

    return suggests
