from django_elasticsearch_dsl import DocType, Index, fields
from elasticsearch_dsl import token_filter, analyzer
from .models import Company, Location


# Name of the Elasticsearch index
company = Index('companies')
# See Elasticsearch Indices API reference for available settings
company.settings(
    number_of_shards=1,
    number_of_replicas=0
)


synonym_tokenfilter = token_filter(
    'synonym_tokenfilter',
    'synonym',
    synonyms=[
        'organisation => company',
        'artificial intelligence => ai',
        'blockchain => crypto',
        'money => payment'
    ]
)


text_analyzer = analyzer(
    'text_analyzer',
    tokenizer='standard',
    filter=[
        # The ORDER is important here.
        'standard',
        'lowercase',
        'stop',
        synonym_tokenfilter,
        # Note! 'snowball' comes after 'synonym_tokenfilter'
        'snowball',
    ],
    char_filter=['html_strip']
)


@company.doc_type
class CompanyDocument(DocType):

    name = fields.TextField(
        fields={'raw': fields.CompletionField()},
        analyzer=text_analyzer
    )

    short_description = fields.TextField(
        fields={'raw': fields.CompletionField()},
        analyzer=text_analyzer
    )

    category_list = fields.NestedField(properties={
        'name': fields.TextField(
            fields={
                'raw': fields.CompletionField()
            }
        )
    })

    category_group_list = fields.NestedField(properties={
        'name': fields.TextField(
            fields={
                'raw': fields.CompletionField()
            }
        )
    })

    founded = fields.DateField(
        format='yyyy-MM-dd'
    )

    location = fields.NestedField(properties={
        'city': fields.TextField(
            fields={
                'keyword': fields.Keyword()
            }
        ),
        'area': fields.TextField(
            fields={
                'keyword': fields.Keyword()
            }
        ),
        'country': fields.TextField(
            fields={
                'keyword': fields.Keyword()
            }
        ),
        'continent': fields.TextField(
            fields={
                'keyword': fields.Keyword()
            }
        )
    })
    
    class Meta:
        model = Company  # The model associated with this DocType
        # The fields of the model you want to be indexed in Elasticsearch
        fields = [
            'cb_url',
            'homepage',
            'status',
            'last_funding_on',
            'funding_total_usd',
            'funding_rounds'
        ]
        # Ignore auto updating of Elasticsearch when a model is saved
        # or deleted:
        # ignore_signals = True
        # Don't perform an index refresh after every update (overrides global setting):
        # auto_refresh = False
        # Paginate the django queryset used to populate the index with the specified size
        # (by default there is no pagination)
        # queryset_pagination = 5000
        related_models = [Company, Location]

    def get_instances_from_related(self, related_instance):
        if isinstance(related_instance, Location):
            return related_instance.company.all()
