from django.conf.urls import url, include
from rest_framework import routers
from startship.search import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from .views import index

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'locations', views.LocationViewSet)
router.register(r'countries', views.CountryViewSet)
router.register(r'categories', views.CategoryViewSet)
router.register(r'category-groups', views.CategoryGroupViewSet)
# router.register(r'companies', views.CompanyViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.

urlpatterns = [
    url(r'^$', index),
    url(r'^api/', include(router.urls)),
    url(r'^api/companies/$', views.search_companies),
    url(r'^admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
