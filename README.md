## Startship

# Requirements

1. Installed `docker`, `docker-compose` and `make`

# Getting Started

1. Run `docker-compose build`
1. Run `docker-compose up -d`
1. Run `make install`
1. Run `make makemigrations`
1. Run `make migrate`
1. Run `make createsuperuser`
1. Run `make collectstatic`

For restarting the server:
* Run `make restart`


